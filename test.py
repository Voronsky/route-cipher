from routecipher import RouteCipher


test1 = "SNXXXXXXXXXUILIKETHEOOMDNAN"
test2 = "ETXSST"
test3 = "T"
test4 = "SHSTAHWCATTI"
test5 = "CEXXECNOTAEOWEAREDISLFDEREV"

def test():
    obj = RouteCipher()
    assert obj.encrypt('I like the sun and moon','(9,3)','clockwise') == test1, \
        "Cipher produced the wrong transposition output"
    assert obj.encrypt('tests','(2,3)','clockwise') == test2, \
        "Cipher produced the wrong transposition output"
    assert obj.encrypt('tests','(1,1)','clockwise') == test3, \
        "Cipher produced the wrong transposition output"
    assert obj.encrypt('Cats,with,hats.','(4,3)','clockwise') == test4, \
        "Cipher produced the wrong transposition output"
    assert obj.encrypt('WE ARE DISCOVERED. FLEE AT ONCE','(9,3)','clockwise') == test5, \
        "Cipher produced the wrong transposition output"

    obj.encrypt('Test the water','(2,1)','left') #raise the Exception


if __name__ == '__main__':
   test() 


   """
    I L I K E T H E S 
    U N A N D M O O N 
    X X X X X X X X X

    output = SNXXXXXXXXXUILIKETHEOOMDNAN
   """