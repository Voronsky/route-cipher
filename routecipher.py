from ast import literal_eval

class Error(Exception):
    pass

class SpiralError(Error):
    """Exception raised for the errors in the direction to spiral

    Attributes:
        direction -- the direction that caused the error
        message -- the message to explain the error
    """
    def __init__(self,direction,message):
        self.direction = direction
        self.message = message

class RouteCipher(object):

    def __init__(self):
        pass

    def encrypt(self,string,dimensions,spiral_rotation):
        """ Creates the grid based on the user's grid dimensions, then uses the 
        spiral rotation direction to create the transposed string

        Returns the encrypted string based on the spiral direction

        Keyword Arguments:
        string -- the String to be encrytped
        dimensions -- a tuple that contains the (columns, rows)
        spiral_rotation -- a direction for rotation: clockwise or counter-clockwise
        """
        self.string = string.replace(',',"").replace('.',"").replace(" ","").upper()
        self.dimensions = literal_eval(dimensions)
        self.cols = literal_eval(dimensions)[0]
        self.rows = literal_eval(dimensions)[1]
        self.eStr = ""
        self.spiral_rotation = spiral_rotation
        self.grid = []

        self.k = 0 #starting column index
        self.j = 0 #starting row
        self.m = self.cols #ending column index
        self.n = self.rows #ending row

        #print(self.string)

        # Create the grid based on user specified dimensions, fill with Xs
        for row in range(self.rows):
            self.chars = []
            if(len(self.string)<self.cols):
                print("Less than cols")
                self.string = self.string+('X'*(self.cols-len(self.string)))
                for i in range(len(self.string)):
                    self.chars.append(self.string[i])
                row = self.chars
                self.grid.append(row)
                self.string = "X"*self.cols
            else:
                print("Equal to or more than cols")
                #row = self.string[:self.cols]
                for i in range(len(self.string[:self.cols])):
                    self.chars.append(self.string[i])
                row = self.chars
                print(row)
                self.grid.append(row)
                self.string = self.string[self.cols:]
                print(self.string)

        for rows in self.grid:
            print(rows)

        print(self.grid)

        if spiral_rotation is "clockwise": #Start top right
            while(self.k < self.rows and self.j < self.cols):
                print(self.cols,self.rows)
                print(self.j,self.k)

                #for each row print the last column index element
                for i in range(self.k,self.rows): 
                    self.eStr = self.eStr + self.grid[i][self.cols-1]
            
                self.cols -= 1 #shift the end column index left

                #if(self.k < self.rows):
                    
                #read the last row and any remaining rows
                for i in range(self.cols-1,self.j-1,-1): #loop in reverse
                                                            # j-1 handles the 0'th element
                    self.eStr = self.eStr+self.grid[self.rows-1][i]

                self.rows -= 1 #shift up the end row index up

                #read the first column (read up), we need to count in reverse
                #read remaining columns afterwords
                if(self.j < self.cols):
                    for i in range(self.rows-1,self.k-1,-1):
                        self.eStr = self.eStr+self.grid[i][self.j]

                    self.j += 1 #increment starting column index

                #read the first row and any remaining rows
                #only if we have not already read it because it was the "last" row
                if(self.k < self.rows):
                    for i in range(self.j,self.cols):
                     self.eStr = self.eStr + self.grid[self.k][i]

                    self.k += 1 #increment starting row index

        elif spiral_rotation is 'counter-clockwise': #counterclockwise, start bottom right

            # read from the last column, bottom right, go up
            # Then read from every "last column"
            for i in range(self.rows-1, self.k-1,-1):
                self.eStr = self.eStr + self.grid[i][self.cols-1]

            self.cols -= 1

            if(self.k < self.rows):
                #Read from the top row, right to left
                #Then set the starting row index to the next one
                for i in range(self.cols-1,self.j-1,-1): 
                    self.eStr = self.eStr + self.grid[self.k][i]

                self.k +=1
            
            if(self.j < self.cols):
                # read the first column
                # and every other "first column"
                # Then set the starting column index over one
                for i in range(self.k,self.rows):
                    self.eStr = self.eStr+self.grid[i][self.j]

                self.j += 1
            # read from the last row
            # and every other "last row"
            for i in range(self.j,self.cols):
                self.eStr = self.eStr + self.grid[self.rows][i]

            self.rows -= 1

        else:
            raise SpiralError(spiral_rotation,
                "Rotation must be clockwise or counter-clockwise, zigzag not yet supported ")

        #print(self.eStr)
        return self.eStr